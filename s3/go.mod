module gitlab.com/zenyuk_go/go_aws/s3

go 1.12

require (
	github.com/aws/aws-sdk-go v1.19.0
	github.com/sirupsen/logrus v1.4.0
)
