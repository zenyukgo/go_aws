package main // gitlab.com/zenyuk_go/go_aws/s3

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	log "github.com/sirupsen/logrus"
)

func main() {
	session, err := session.NewSession(&aws.Config{Region: aws.String("ap-southeast-2")})
	if err != nil {
		log.Error("Can't establish AWS session")
	}
	s3Service := s3.New(session)
	buckets, err := s3Service.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		log.Error()
	}
	for _, b := range buckets.Buckets {
		fmt.Printf("bucket: %v\t, created: %v\n", *b.Name, b.CreationDate.Format("02-Feb-2006"))
	}
}
