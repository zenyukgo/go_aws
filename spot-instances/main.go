package main // gitlab.com/zenyuk_go/go_aws/spot-instances

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	log "github.com/sirupsen/logrus"
)

// AwsSpotPrice prices
type AwsSpotPrice struct {
	AvailabilityZone   string
	InstanceType       string
	ProductDescription string
	SpotPrice          float64
	Timestamp          time.Time
}

func main() {
	awsRegion := "ap-southeast-2"
	awsZone := awsRegion + "a"
	awsInstanceType := "t2.micro"

	awsSession, err := session.NewSession(&aws.Config{
		Region: &awsRegion},
	)

	price, err := getCheapestPrice(awsSession, &awsInstanceType, &awsZone)
	if err != nil {
		log.Error(err)
	}
	fmt.Printf("lowest price: %f\n", price.SpotPrice)
}

func getCheapestPrice(awsSession *session.Session, awsInstanceType *string, awsZone *string) (result AwsSpotPrice, err error) {
	result.SpotPrice = math.MaxFloat64

	ec2Service := ec2.New(awsSession)
	now := time.Now()
	before := now.Add(time.Minute * time.Duration(-30))
	input := &ec2.DescribeSpotPriceHistoryInput{
		EndTime:          &now,
		InstanceTypes:    []*string{awsInstanceType},
		AvailabilityZone: awsZone,
		StartTime:        &before,
	}

	pricesResult, err := ec2Service.DescribeSpotPriceHistory(input)
	if err != nil {
		return result, fmt.Errorf("error getting prices: %+v", err)
	}
	if len(pricesResult.SpotPriceHistory) == 0 {
		return result, errors.New("no prices")
	}

	for _, p := range pricesResult.SpotPriceHistory {
		price, err := strconv.ParseFloat(*p.SpotPrice, 64)
		if err != nil {
			return result, fmt.Errorf("can't parse price: %+v", *p.SpotPrice)
		}
		if price < result.SpotPrice {
			result.SpotPrice = price
			result.AvailabilityZone = *p.AvailabilityZone
			result.InstanceType = *p.InstanceType
			result.ProductDescription = *p.ProductDescription
			result.Timestamp = *p.Timestamp
		}
	}
	return
}
