module gitlab.com/zenyuk_go/go_aws/spot-instances

go 1.12

require (
	github.com/aws/aws-sdk-go v1.19.2
	github.com/sirupsen/logrus v1.4.0
)
