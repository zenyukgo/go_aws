package main // gitlab.com/zenyuk_go/go_aws/lambda

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elb"
)

var loadBalancerName = "InterchangeSpotIt"

// DeregisterFromLoadbalancer - remove from load balancer
func DeregisterFromLoadbalancer(awsSession *session.Session, instanceID string) error {
	elbInstance := elb.Instance{
		InstanceId: &instanceID,
	}
	awsLoadBalancerService := elb.New(awsSession)

	_, err := awsLoadBalancerService.DeregisterInstancesFromLoadBalancer(&elb.DeregisterInstancesFromLoadBalancerInput{
		LoadBalancerName: &loadBalancerName,
		Instances:        []*elb.Instance{&elbInstance},
	})
	return err
}

// RegisterNewInstanceWithLoadbalancer - add to load balancer
func RegisterNewInstanceWithLoadbalancer(awsSession *session.Session, instanceID string) error {
	elbInstance := elb.Instance{
		InstanceId: &instanceID,
	}
	awsLoadBalancerService := elb.New(awsSession)

	_, err := awsLoadBalancerService.RegisterInstancesWithLoadBalancer(&elb.RegisterInstancesWithLoadBalancerInput{
		LoadBalancerName: &loadBalancerName,
		Instances:        []*elb.Instance{&elbInstance},
	})
	return err
}
